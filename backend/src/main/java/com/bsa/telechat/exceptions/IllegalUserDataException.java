package com.bsa.telechat.exceptions;

public class IllegalUserDataException extends Exception {
    public IllegalUserDataException(String message) {
        super(message);
    }
}
