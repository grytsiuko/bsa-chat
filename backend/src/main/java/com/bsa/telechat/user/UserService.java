package com.bsa.telechat.user;

import com.bsa.telechat.auth.AuthUser;
import com.bsa.telechat.exceptions.AdminStatusRequiredException;
import com.bsa.telechat.exceptions.IllegalUserDataException;
import com.bsa.telechat.user.dto.UserDetailedDto;
import com.bsa.telechat.user.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.bsa.telechat.auth.TokenService.getUserId;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserDto> getAll() throws AdminStatusRequiredException {
        assertAmin();
        return userRepository
                .findAllBy()
                .stream()
                .map(UserDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<UserDto> getOne(UUID id) {
        return userRepository
                .findById(id)
                .map(UserDto::fromEntity);
    }

    public Optional<UserDto> getCurrent() {
        return userRepository
                .findById(getUserId())
                .map(UserDto::fromEntity);
    }

    public void add(UserDetailedDto detailedDto) throws IllegalUserDataException, AdminStatusRequiredException {
        assertAmin();
        if (userRepository.findByUsername(detailedDto.getUsername()).isPresent()) {
            throw new IllegalUserDataException("User with such name exists");
        }

        var user = UserDetailedDto.toEntity(detailedDto);
        if (user.getPassword().isBlank()) {
            throw new IllegalUserDataException("Blank password");
        }
        if (user.getUsername().isBlank()) {
            throw new IllegalUserDataException("Blank username");
        }

        user.setId(null);
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    public void update(UserDetailedDto detailedDto) throws IllegalUserDataException, AdminStatusRequiredException {
        assertAmin();
        var seek = userRepository.findByUsername(detailedDto.getUsername());
        if (seek.isPresent() && !seek.get().getId().equals(detailedDto.getId())) {
            throw new IllegalUserDataException("User with such name exists");
        }
        if (detailedDto.getUsername().isBlank()) {
            throw new IllegalUserDataException("Blank username");
        }

        var original = userRepository.findById(detailedDto.getId()).orElseThrow();

        original.setUsername(detailedDto.getUsername());
        original.setAvatar(detailedDto.getAvatar());
        original.setIsAdmin(detailedDto.getIsAdmin());
        if (!detailedDto.getPassword().equals("")) {
            original.setPassword(bCryptPasswordEncoder.encode(detailedDto.getPassword()));
        }
        userRepository.save(original);
    }

    public void delete(UUID id) throws AdminStatusRequiredException {
        assertAmin();
        var user = userRepository.findById(id).orElseThrow();
        userRepository.delete(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository
                .findByUsername(username)
                .map(u -> new AuthUser(u.getId(), u.getUsername(), u.getPassword()))
                .orElseThrow();
    }

    private void assertAmin() throws AdminStatusRequiredException {
        var currentUser = userRepository.findById(getUserId()).orElseThrow();
        if (!currentUser.getIsAdmin()) {
            throw new AdminStatusRequiredException();
        }
    }
}
