package com.bsa.telechat.user.dto;

import com.bsa.telechat.user.User;
import lombok.Builder;
import lombok.Data;
import lombok.AllArgsConstructor;

import java.util.UUID;

@Builder
@Data
@AllArgsConstructor
public class UserDto {
    private final UUID id;
    private final String username;
    private final String avatar;
    private final Boolean isAdmin;

    public static UserDto fromEntity(User user) {
        return UserDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .avatar(user.getAvatar())
                .isAdmin(user.getIsAdmin())
                .build();
    }
}
