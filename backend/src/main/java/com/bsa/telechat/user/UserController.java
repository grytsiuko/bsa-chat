package com.bsa.telechat.user;

import com.bsa.telechat.exceptions.AdminStatusRequiredException;
import com.bsa.telechat.exceptions.IllegalUserDataException;
import com.bsa.telechat.user.dto.UserDetailedDto;
import com.bsa.telechat.user.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    private UserService userService;

    @ExceptionHandler({AdminStatusRequiredException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public String handleIllegalLoginDataException() {
        return "Admin status required";
    }

    @ExceptionHandler({IllegalUserDataException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public String handleIllegalUserDataException(IllegalUserDataException e) {
        return e.getMessage();
    }

    @GetMapping
    public List<UserDto> getAll() throws AdminStatusRequiredException {
        return userService.getAll();
    }

    @GetMapping("/current")
    public UserDto getCurrent() {
        return userService.getCurrent().orElseThrow();
    }

    @PostMapping
    public void add(@RequestBody UserDetailedDto detailedDto) throws IllegalUserDataException, AdminStatusRequiredException {
        userService.add(detailedDto);
    }

    @PutMapping
    public void update(@RequestBody UserDetailedDto detailedDto) throws IllegalUserDataException, AdminStatusRequiredException {
        userService.update(detailedDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id) throws AdminStatusRequiredException {
        userService.delete(id);
    }
}
