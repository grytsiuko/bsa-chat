package com.bsa.telechat.user;

import com.bsa.telechat.message.Message;
import com.bsa.telechat.messageReactions.MessageReaction;
import org.hibernate.annotations.GenericGenerator;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column
    private String username;

    @Column
    private String password;

    @Column
    private String avatar;

    @Column
    private Boolean isAdmin;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Message> messages = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<MessageReaction> reactions = new ArrayList<>();

    public User(String username, String password, String avatar, Boolean isAdmin) {
        this.username = username;
        this.password = password;
        this.avatar = avatar;
        this.isAdmin = isAdmin;
    }
}
