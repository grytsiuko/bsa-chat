package com.bsa.telechat.user.dto;

import com.bsa.telechat.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Builder
@Data
@AllArgsConstructor
public class UserDetailedDto {
    private final UUID id;
    private final String username;
    private final String avatar;
    private final Boolean isAdmin;
    private final String password;

    public static User toEntity(UserDetailedDto detailedDto) {
        return new User(
                detailedDto.getId(),
                detailedDto.getUsername(),
                detailedDto.getPassword(),
                detailedDto.getAvatar(),
                detailedDto.getIsAdmin(),
                null, null
        );
   }
}
