package com.bsa.telechat.message;

import com.bsa.telechat.message.dto.MessageCreateDto;
import com.bsa.telechat.message.dto.MessageDetailedDto;
import com.bsa.telechat.message.dto.MessageUpdateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/messages")
public class MessageController {
    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping
    public List<MessageDetailedDto> getAll() {
        return messageService.getAll();
    }

    @PostMapping
    public MessageDetailedDto add(@RequestBody MessageCreateDto createDto) {
        return messageService.create(createDto);
    }

    @PutMapping
    public MessageDetailedDto update(@RequestBody MessageUpdateDto updateDto) {
        return messageService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id) {
        messageService.delete(id);
    }
}
