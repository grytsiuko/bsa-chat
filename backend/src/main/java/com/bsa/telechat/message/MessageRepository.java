package com.bsa.telechat.message;

import com.bsa.telechat.message.dto.MessageDetailedDto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MessageRepository extends CrudRepository<Message, UUID> {

    @Query("SELECT new com.bsa.telechat.message.dto.MessageDetailedDto( " +
            "      m.id, " +
            "      m.user.id, " +
            "      m.user.username, " +
            "      m.text, " +
            "      m.user.avatar, " +
            "      m.createdAt, " +
            "      m.editedAt, " +
            "      m.reactions.size, " +
            "      (EXISTS (SELECT r FROM m.reactions r WHERE r.user.id = :currentUserId))" +
            "   ) " +
            "FROM Message m")
    List<MessageDetailedDto> findDetailedList(UUID currentUserId);

    List<Message> findAllByOrderByCreatedAt();
}
