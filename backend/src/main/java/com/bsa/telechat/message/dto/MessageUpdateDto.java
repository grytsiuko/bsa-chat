package com.bsa.telechat.message.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Builder
@Data
@AllArgsConstructor
public class MessageUpdateDto {
    private final UUID id;
    private final String text;
}
