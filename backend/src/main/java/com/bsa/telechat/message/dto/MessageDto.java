package com.bsa.telechat.message.dto;

import com.bsa.telechat.message.Message;
import com.bsa.telechat.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Builder
@Data
@AllArgsConstructor
public class MessageDto {
    private final UUID id;
    private final UUID userId;
    private final String user;
    private final String text;
    private final String avatar;
    private final Date createdAt;
    private final Date editedAt;

    public static MessageDto fromEntity(Message message) {
        return MessageDto.builder()
                .id(message.getId())
                .userId(message.getUser().getId())
                .user(message.getUser().getUsername())
                .text(message.getText())
                .avatar(message.getUser().getAvatar())
                .createdAt(message.getCreatedAt())
                .editedAt(message.getEditedAt())
                .build();
    }
}
