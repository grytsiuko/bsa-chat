package com.bsa.telechat.message.dto;

import com.bsa.telechat.message.Message;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageCreateDto {
    private String text;
}
