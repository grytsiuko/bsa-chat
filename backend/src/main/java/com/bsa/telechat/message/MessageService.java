package com.bsa.telechat.message;

import com.bsa.telechat.message.dto.MessageCreateDto;
import com.bsa.telechat.message.dto.MessageDetailedDto;
import com.bsa.telechat.message.dto.MessageUpdateDto;
import com.bsa.telechat.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static com.bsa.telechat.auth.TokenService.getUserId;

@Service
public class MessageService {

    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private UserRepository userRepository;

    public List<MessageDetailedDto> getAll() {
        return messageRepository
                .findDetailedList(getUserId());
    }

    public MessageDetailedDto create(MessageCreateDto createDto) {
        var currentUser = userRepository.findById(getUserId()).orElseThrow();
        var message = new Message(createDto.getText(), currentUser);
        return MessageDetailedDto.fromEntity(messageRepository.save(message));
    }

    public MessageDetailedDto update(MessageUpdateDto updateDto) {
        var message = messageRepository.findById(updateDto.getId()).orElseThrow();
        if (!message.getUser().getId().equals(getUserId()))
            return null;

        message.setText(updateDto.getText());
        return MessageDetailedDto.fromEntity(messageRepository.save(message));
    }

    public void delete(UUID id) {
        var message = messageRepository.findById(id).orElseThrow();
        if (message.getUser().getId().equals(getUserId())) {
            messageRepository.delete(message);
        }
    }
}
