package com.bsa.telechat.message.dto;

import com.bsa.telechat.message.Message;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Builder
@Data
@AllArgsConstructor
public class MessageDetailedDto {
    private final UUID id;
    private final UUID userId;
    private final String user;
    private final String text;
    private final String avatar;
    private final Date createdAt;
    private final Date editedAt;
    private final Integer likes;
    private final Boolean likedByMe;

    public static MessageDetailedDto fromEntity(Message message) {
        return MessageDetailedDto.builder()
                .id(message.getId())
                .userId(message.getUser().getId())
                .user(message.getUser().getUsername())
                .text(message.getText())
                .avatar(message.getUser().getAvatar())
                .createdAt(message.getCreatedAt())
                .editedAt(message.getEditedAt())
                .likes(message.getReactions().size())
                .likedByMe(false)
                .build();
    }
}
