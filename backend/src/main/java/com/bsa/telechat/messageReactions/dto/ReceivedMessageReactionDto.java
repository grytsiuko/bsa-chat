package com.bsa.telechat.messageReactions.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ReceivedMessageReactionDto {
    private UUID messageId;
    private UUID userId;
}
