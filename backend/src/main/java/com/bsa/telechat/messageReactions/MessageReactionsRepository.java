package com.bsa.telechat.messageReactions;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface MessageReactionsRepository extends CrudRepository<MessageReaction, UUID> {
    @Query("SELECT r " +
            "FROM MessageReaction r " +
            "WHERE r.user.id = :userId AND r.message.id = :messageId ")
    Optional<MessageReaction> getMessageReaction(@Param("userId") UUID userId, @Param("messageId") UUID messageId);

    Integer countByMessageId(UUID messageId);
}