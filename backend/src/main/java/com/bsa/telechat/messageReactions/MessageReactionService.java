package com.bsa.telechat.messageReactions;

import com.bsa.telechat.message.MessageRepository;
import com.bsa.telechat.messageReactions.dto.ReceivedMessageReactionDto;
import com.bsa.telechat.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageReactionService {
    @Autowired
    private MessageReactionsRepository messageReactionsRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessageRepository messageRepository;

    public Integer setReaction(ReceivedMessageReactionDto reactionDto) {

        var reaction = messageReactionsRepository.getMessageReaction(reactionDto.getUserId(), reactionDto.getMessageId());

        if (reaction.isPresent()) {
            var react = reaction.get();
            messageReactionsRepository.delete(react);
        } else {
            var user = userRepository.findById(reactionDto.getUserId()).orElseThrow();
            var message = messageRepository.findById(reactionDto.getMessageId()).orElseThrow();
            messageReactionsRepository.save(new MessageReaction(user, message));
        }

        return messageReactionsRepository.countByMessageId(reactionDto.getMessageId());
    }
}
