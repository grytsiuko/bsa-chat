package com.bsa.telechat.messageReactions;

import com.bsa.telechat.messageReactions.dto.ReceivedMessageReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.bsa.telechat.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/messages/reactions")
public class MessageReactionController {

    @Autowired
    private MessageReactionService messageReactionService;

    @PutMapping
    public Integer setReaction(@RequestBody ReceivedMessageReactionDto reactionDto) {
        reactionDto.setUserId(getUserId());
        return messageReactionService.setReaction(reactionDto);
    }
}
