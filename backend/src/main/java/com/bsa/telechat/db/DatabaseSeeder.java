package com.bsa.telechat.db;

import com.bsa.telechat.message.Message;
import com.bsa.telechat.message.MessageRepository;
import com.bsa.telechat.messageReactions.MessageReaction;
import com.bsa.telechat.messageReactions.MessageReactionsRepository;
import com.bsa.telechat.user.User;
import com.bsa.telechat.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class DatabaseSeeder {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private MessageReactionsRepository messageReactionsRepository;
    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        seedUsers();
        seedMessages();
    }

    private void seedUsers() {
        userRepository.save(new User("admin", bCryptPasswordEncoder.encode("admin"), "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", true));
        userRepository.save(new User("lana", bCryptPasswordEncoder.encode("lana"), "https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng", false));
        userRepository.save(new User("kate", bCryptPasswordEncoder.encode("kate"), "https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ", false));
        userRepository.save(new User("ben", bCryptPasswordEncoder.encode("ben"), "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg", false));
    }

    private void seedMessages() {
        var admin = userRepository.findByUsername("admin").orElseThrow();
        var ben = userRepository.findByUsername("ben").orElseThrow();

        messageRepository.save(new Message("Hello", admin));
        var adminMessage = messageRepository.save(new Message("I am admin", admin));
        var benMessage = messageRepository.save(new Message("Hello, admin!", ben));

        messageReactionsRepository.save(new MessageReaction(admin, benMessage));
        messageReactionsRepository.save(new MessageReaction(ben, adminMessage));
    }

}
