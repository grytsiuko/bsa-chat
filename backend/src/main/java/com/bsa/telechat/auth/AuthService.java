package com.bsa.telechat.auth;

import com.bsa.telechat.auth.dto.AuthUserDto;
import com.bsa.telechat.auth.dto.UserLoginDto;
import com.bsa.telechat.exceptions.IllegalLoginDataException;
import com.bsa.telechat.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private UserService userService;

    public AuthUserDto login(UserLoginDto user) throws Exception {
        Authentication auth;
        try {
            auth = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword())
            );
        } catch (Exception e) {
            throw new IllegalLoginDataException();
        }

        var currentUser = (AuthUser) auth.getPrincipal();
        var userDetails = userService.getOne(currentUser.getId()).orElseThrow();
        var jwt = tokenService.generateToken(currentUser);
        return new AuthUserDto(jwt, userDetails);
    }
}