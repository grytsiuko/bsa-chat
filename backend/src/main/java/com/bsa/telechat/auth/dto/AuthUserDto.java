package com.bsa.telechat.auth.dto;

import com.bsa.telechat.user.dto.UserDto;
import lombok.Data;
import lombok.AllArgsConstructor;

@Data
@AllArgsConstructor
public class AuthUserDto {
    private String token;
    private UserDto user;
}
