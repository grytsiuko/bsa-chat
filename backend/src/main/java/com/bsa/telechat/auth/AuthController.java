package com.bsa.telechat.auth;

import com.bsa.telechat.auth.dto.AuthUserDto;
import com.bsa.telechat.auth.dto.UserLoginDto;
import com.bsa.telechat.exceptions.IllegalLoginDataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private AuthService authService;

    @ExceptionHandler({IllegalLoginDataException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public String handleIllegalLoginDataException() {
        return "No user with such data";
    }

    @PostMapping("/login")
    public AuthUserDto login(@RequestBody UserLoginDto user)
            throws Exception {

        return authService.login(user);
    }
}
