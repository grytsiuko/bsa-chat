package com.bsa.telechat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TelechatApplication {

	public static void main(String[] args) {
		SpringApplication.run(TelechatApplication.class, args);
	}

}
