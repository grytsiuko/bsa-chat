import {combineReducers, createStore, applyMiddleware} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import {all} from 'redux-saga/effects';

import chatReducer, {IChatState} from "./containers/Chat/reducer";
import loginReducer, {ILoginState} from "./containers/LoginPage/reducer";
import editReducer, {IEditState} from "./containers/EditPage/reducer";
import usersReducer, {IUsersState} from "./containers/UsersPage/reducer";
import editUserReducer, {IEditUserState} from "./containers/EditUserPage/reducer";
import messageSagas from "./containers/Chat/sagas";
import loginSagas from "./containers/LoginPage/sagas";
import usersSagas from "./containers/UsersPage/sagas";
import appReducer, {IAppState} from "./containers/App/reducer";

export interface IRootState {
  chat?: IChatState
  login?: ILoginState
  edit?: IEditState
  users?: IUsersState
  editUser?: IEditUserState,
  app?: IAppState
}

const rootReducer = combineReducers({
  chat: chatReducer,
  login: loginReducer,
  edit: editReducer,
  users: usersReducer,
  editUser: editUserReducer,
  app: appReducer
});

const sagaMiddleware = createSagaMiddleware();
const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(sagaMiddleware)));

function* rootSaga() {
  yield all([
    messageSagas(),
    loginSagas(),
    usersSagas()
  ]);
}

sagaMiddleware.run(rootSaga);

export default store;
