import React from 'react';

import { IMessage } from '../../service/messageService';

type MessageAvatarProps = {
  message: IMessage
}

const MessageAvatar: React.FunctionComponent<MessageAvatarProps> = ({ message }) => (
  <div className="message-avatar-wrapper">
    <img className="message-avatar" alt={message.user} src={message.avatar} />
  </div>
);

export default MessageAvatar;
