import React from 'react';

import './index.css';

type SimpleButtonProps = {
  title: string,
  callback(): void
}

const SimpleButton: React.FunctionComponent<SimpleButtonProps> = ({ title, callback }) => (
  <button type="button" className="simple-button button" onClick={callback}>
    {title}
  </button>
);

export default SimpleButton;
