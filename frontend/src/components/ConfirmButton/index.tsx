import React from 'react';

import './index.css';

type ConfirmButtonProps = {
  title: string,
  callback(): void,
  isBlocked: boolean,
  submit?: boolean
}

const ConfirmButton: React.FunctionComponent<ConfirmButtonProps> = ({ title, callback, isBlocked, submit}) => {
  const className: string = `confirm-button button${isBlocked ? ' button-blocked' : ''}`;
  const type: 'submit' | 'button' = submit ? 'submit' : 'button';

  return (
    <button type={type} className={className} onClick={callback}>
      {title}
    </button>
  );
};

export default ConfirmButton;
