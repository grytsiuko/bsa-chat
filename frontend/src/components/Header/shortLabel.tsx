import React from 'react';

type HeaderShortLabelProps = {
  label: string,
  text: string
}

const HeaderShortLabel: React.FunctionComponent<HeaderShortLabelProps> = ({ label, text }) => (
  <div className="header-block header-info">
    <span className="header-number">{label}</span>
    {' '}
    {text}
  </div>
);

export default HeaderShortLabel;
