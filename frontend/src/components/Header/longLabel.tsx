import React from 'react';

type HeaderLongLabelProps = {
  label: string,
  text: string
}

const HeaderLongLabel: React.FunctionComponent<HeaderLongLabelProps> = ({ label, text }) => (
  <div className="header-block header-info">
    {text}
    {' '}
    <br className="header-info-br" />
    <span className="header-number">{label}</span>
  </div>
);

export default HeaderLongLabel;
