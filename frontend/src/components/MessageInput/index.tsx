import React, { useEffect, useRef, useState } from 'react';

import './index.css';
import ConfirmButton from '../ConfirmButton';

type MessageInputProps = {
  addMessage(text: string): void
  editLast(): void
}

const MessageInput: React.FunctionComponent<MessageInputProps> = ({ addMessage, editLast }) => {
  const [newMessageText, setNewMessageText] = useState<string>('');
  const textareaRef = useRef<HTMLTextAreaElement>(null);

  const handleChange = (value: string): void => {
    setNewMessageText(value);
  };

  const handleSend = (): void => {
    if (newMessageText.trim()) {
      addMessage(newMessageText);
      setNewMessageText('');
    }
  };

  const handleTextAreaKeyPress = (e: React.KeyboardEvent<HTMLTextAreaElement>): void => {
    if (e.key === 'Enter' && !e.shiftKey) {
      e.preventDefault();
      handleSend();
    }
  };

  const handleKeyboardPress = (e: KeyboardEvent): void => {
    if (e.key === 'ArrowUp' && !newMessageText) {
      e.preventDefault();
      editLast();
    }
  };

  useEffect(() => {
    textareaRef.current?.focus();
    document.addEventListener('keydown', handleKeyboardPress);
    return () => {
      document.removeEventListener('keydown', handleKeyboardPress);
    };
  });

  return (
    <div className="message-input">
      <textarea
        className="message-input-area"
        placeholder="Type your message here. Press Enter to confirm, Shift+Enter to switch to a new line."
        onChange={e => handleChange(e.target.value)}
        onKeyDown={e => handleTextAreaKeyPress(e)}
        value={newMessageText}
        ref={textareaRef}
      />
      <div className="message-input-button-wrapper">
        <ConfirmButton
          callback={() => handleSend()}
          isBlocked={newMessageText.trim() === ''}
          title="Send"
        />
      </div>
    </div>
  );
};

export default MessageInput;
