import React from 'react';

import './index.css';

type ListSeparatorProps = {
  title: string,
}

const ListSeparator: React.FunctionComponent<ListSeparatorProps> = ({ title }) => (
  <div className="list-separator-wrapper">
    <div className="list-separator-title">
      {title}
    </div>
  </div>
);

export default ListSeparator;
