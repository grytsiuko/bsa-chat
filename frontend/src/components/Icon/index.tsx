import React from 'react';

import './index.css';

type IconProps = {
  icon: string,
  active: boolean,
  callback(): void
}

const Icon: React.FunctionComponent<IconProps> = ({ icon, active, callback }) => (
  <span className={`icon ${icon}${active ? ' icon-active' : ''}`} onClick={callback} />
);

export default Icon;
