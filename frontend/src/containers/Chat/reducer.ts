import { AnyAction } from 'redux';
import { IMessage, IMessageArray } from '../../service/messageService';
import {
  ADD_MESSAGE_SUCCEEDED,
  DELETE_MESSAGE_SUCCEEDED,
  LOAD_MESSAGES_SUCCEEDED,
  SET_EDITING_MESSAGE_SUCCEEDED,
  UPDATE_MESSAGE_SUCCEEDED
} from './actionTypes';
import {LOGOUT_USER_SUCCEEDED} from "../LoginPage/actionTypes";

export interface IChatState {
  messages?: IMessageArray,
  scrollOnRender?: boolean,
  editingMessage?: IMessage
}

const chatReducer = (state: IChatState = {}, action: AnyAction) => {
  switch (action.type) {
    case LOAD_MESSAGES_SUCCEEDED:
      return {
        ...state,
        messages: action.messages,
        scrollOnRender: action.scrollOnRender
      };
    case ADD_MESSAGE_SUCCEEDED:
      return {
        ...state,
        messages: [...(state.messages || []), action.message],
        scrollOnRender: action.scrollOnRender
      };
    case UPDATE_MESSAGE_SUCCEEDED:
      return {
        ...state,
        messages: (state.messages || []).map(m => (m.id === action.message.id ? action.message : m)),
        scrollOnRender: action.scrollOnRender
      };
    case DELETE_MESSAGE_SUCCEEDED:
      return {
        ...state,
        messages: (state.messages || []).filter(m => m.id !== action.id),
        scrollOnRender: action.scrollOnRender
      };
    case SET_EDITING_MESSAGE_SUCCEEDED:
      return {
        ...state,
        editingMessage: action.message,
        scrollOnRender: action.scrollOnRender
      };
    case LOGOUT_USER_SUCCEEDED:
      return {};
    default:
      return state;
  }
};

export default chatReducer;
