import React from 'react';

import './index.css';
import {Dispatch} from 'redux';
import {connect} from 'react-redux';
import MessageList from '../../components/MessageList';
import {IMessage, IMessageArray} from '../../service/messageService';
import {
  loadMessagesRequestAction,
  addMessageRequestAction,
  toggleLikeRequestAction,
  deleteMessageRequestAction
} from './actions';
import {IRootState} from '../../store';
import Header from '../../components/Header';
import MessageInput from '../../components/MessageInput';
import {IUser} from "../../service/userService";
import {useHistory} from "react-router";
import {setEditMessageAction} from "../EditPage/actions";

interface ChatStateProps {
  messages: IMessageArray | undefined,
  scrollOnRender: boolean | undefined,
  user?: IUser
}

interface ChatProps extends ChatStateProps {
  getMessages(): void,
  addMessage(text: string): void,
  toggleLikeMessage(message: IMessage): void
  deleteMessage(id: string): void,
  setEditMessage(message: IMessage): void
}

const Chat: React.FunctionComponent<ChatProps> = (
  {
    messages,
    user,
    getMessages,
    addMessage,
    toggleLikeMessage,
    deleteMessage,
    setEditMessage,
    scrollOnRender
  }
) => {
  const history = useHistory();

  if (!messages) {
    getMessages();
  }

  const handleEdit = (message: IMessage): void => {
    setEditMessage(message);
    history.push("/edit");
  }

  const editLastMessage = (): void => {
    const currentUserMessages: IMessageArray = (messages || []).filter(m => m.userId === user?.id);
    const {length} = currentUserMessages;
    if (length > 0) {
      handleEdit(currentUserMessages[length - 1]);
    }
  };

  return (
    <div className="chat-wrapper">
      <div className="chat">
        <Header messages={messages}/>
        <MessageList
          messages={messages}
          user={user}
          deleteMessage={deleteMessage}
          toggleLikeMessage={toggleLikeMessage}
          showEditModal={handleEdit}
          scrollOnRender={scrollOnRender}
        />
        <MessageInput addMessage={addMessage} editLast={editLastMessage}/>
      </div>
    </div>
  );
};

const mapStateToProps = (rootState: IRootState): ChatStateProps => ({
  messages: rootState.chat?.messages,
  scrollOnRender: rootState.chat?.scrollOnRender,
  user: rootState.login?.user
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  getMessages: () => dispatch(loadMessagesRequestAction()),
  addMessage: (text: string) => dispatch(addMessageRequestAction(text)),
  toggleLikeMessage: (message: IMessage) => dispatch(toggleLikeRequestAction(message)),
  deleteMessage: (id: string) => dispatch(deleteMessageRequestAction(id)),
  setEditMessage: (message: IMessage) => dispatch(setEditMessageAction(message))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat);
