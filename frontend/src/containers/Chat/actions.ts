import {AnyAction} from 'redux';
import {IMessage, IMessageArray} from '../../service/messageService';
import {
  ADD_MESSAGE_REQUESTED,
  ADD_MESSAGE_SUCCEEDED, DELETE_MESSAGE_REQUESTED,
  DELETE_MESSAGE_SUCCEEDED,
  LOAD_MESSAGES_REQUESTED,
  LOAD_MESSAGES_SUCCEEDED, TOGGLE_LIKE_REQUESTED,
  UPDATE_MESSAGE_REQUESTED,
  UPDATE_MESSAGE_SUCCEEDED
} from './actionTypes';

// REQUESTED

export const loadMessagesRequestAction = (): AnyAction => ({
  type: LOAD_MESSAGES_REQUESTED
});

export const addMessageRequestAction = (text: string): AnyAction => ({
  type: ADD_MESSAGE_REQUESTED,
  text
});

export const updateMessageRequestAction = (message: IMessage): AnyAction => ({
  type: UPDATE_MESSAGE_REQUESTED,
  message
});

export const toggleLikeRequestAction = (message: IMessage): AnyAction => ({
  type: TOGGLE_LIKE_REQUESTED,
  message
});

export const deleteMessageRequestAction = (id: string): AnyAction => ({
  type: DELETE_MESSAGE_REQUESTED,
  id
});

// SUCCEEDED

export const loadMessagesSucceededAction = (messages: IMessageArray): AnyAction => ({
  type: LOAD_MESSAGES_SUCCEEDED,
  messages,
  scrollOnRender: true
});

export const addMessageSucceededAction = (message: IMessage): AnyAction => ({
  type: ADD_MESSAGE_SUCCEEDED,
  message,
  scrollOnRender: true
});

export const updateMessageSucceededAction = (message: IMessage): AnyAction => ({
  type: UPDATE_MESSAGE_SUCCEEDED,
  message,
  scrollOnRender: true
});

export const toggleLikeMessageSucceededAction = (message: IMessage): AnyAction => ({
  type: UPDATE_MESSAGE_SUCCEEDED,
  message,
  scrollOnRender: false
});

export const deleteMessageSucceededAction = (id: string): AnyAction => ({
  type: DELETE_MESSAGE_SUCCEEDED,
  id,
  scrollOnRender: false
});
