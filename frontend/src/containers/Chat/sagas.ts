import * as messageService from '../../service/messageService';
import {call, put, all, takeEvery} from 'redux-saga/effects';

import {IMessage, IMessageArray} from "../../service/messageService";
import {
  addMessageSucceededAction, deleteMessageSucceededAction,
  loadMessagesSucceededAction,
  toggleLikeMessageSucceededAction,
  updateMessageSucceededAction
} from './actions';
import {
  ADD_MESSAGE_REQUESTED,
  DELETE_MESSAGE_REQUESTED,
  LOAD_MESSAGES_REQUESTED,
  TOGGLE_LIKE_REQUESTED,
  UPDATE_MESSAGE_REQUESTED
} from "./actionTypes";
import {AnyAction} from "redux";
import {hideEditMessageAction} from "../EditPage/actions";
import {setLoadingAction, stopLoadingAction} from "../App/actions";

export function* getMessages() {
  yield put(setLoadingAction());
  const loadedMessages: IMessageArray = yield call(messageService.getMessages);
  loadedMessages.sort((a, b) => (a.createdAt > b.createdAt ? 1 : 0));
  yield put(loadMessagesSucceededAction(loadedMessages));
  yield put(stopLoadingAction());
}

function* watchGetMessages() {
  yield takeEvery(LOAD_MESSAGES_REQUESTED, getMessages);
}

export function* addMessage(action: AnyAction) {
  yield put(setLoadingAction());
  const addedMessage: IMessage = yield call(messageService.addMessage, action.text);
  yield put(addMessageSucceededAction(addedMessage));
  yield put(stopLoadingAction());
}

function* watchAddMessage() {
  yield takeEvery(ADD_MESSAGE_REQUESTED, addMessage);
}

export function* updateMessage(action: AnyAction) {
  yield put(setLoadingAction());
  const updatedMessage: IMessage = yield call(messageService.updateMessage, action.message);
  yield put(updateMessageSucceededAction(updatedMessage));
  yield put(hideEditMessageAction());
  yield put(stopLoadingAction());
}

function* watchUpdateMessage() {
  yield takeEvery(UPDATE_MESSAGE_REQUESTED, updateMessage);
}

export function* toggleLikeMessage(action: AnyAction) {
  yield put(setLoadingAction());
  const message: IMessage = action.message;
  message.likes = yield call(messageService.toggleLikeMessage, message);
  message.likedByMe = !message.likedByMe;
  yield put(toggleLikeMessageSucceededAction(message));
  yield put(stopLoadingAction());
}

function* watchToggleLikeMessage() {
  yield takeEvery(TOGGLE_LIKE_REQUESTED, toggleLikeMessage);
}

export function* deleteMessage(action: AnyAction) {
  yield put(setLoadingAction());
  yield call(messageService.deleteMessage, action.id);
  yield put(deleteMessageSucceededAction(action.id));
  yield put(stopLoadingAction());
}

function* watchDeleteMessage() {
  yield takeEvery(DELETE_MESSAGE_REQUESTED, deleteMessage);
}

export default function* messageSagas() {
  yield all([
    watchGetMessages(),
    watchAddMessage(),
    watchUpdateMessage(),
    watchToggleLikeMessage(),
    watchDeleteMessage()
  ]);
}

