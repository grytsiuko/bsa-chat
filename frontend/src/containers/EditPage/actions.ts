import {AnyAction} from 'redux';
import {IMessage} from '../../service/messageService';
import {SET_MESSAGE} from './actionTypes';

export const setEditMessageAction = (message: IMessage): AnyAction => ({
  type: SET_MESSAGE,
  message
});

export const hideEditMessageAction = (): AnyAction => ({
  type: SET_MESSAGE,
  message: null
});
