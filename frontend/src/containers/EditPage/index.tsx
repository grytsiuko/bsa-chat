import React, {useRef, useState, useEffect} from 'react';

import {IMessage} from '../../service/messageService';
import ConfirmButton from '../../components/ConfirmButton';
import SimpleButton from '../../components/SimpleButton';
import {IRootState} from "../../store";
import {Dispatch} from "redux";
import {connect} from "react-redux";
import {Redirect} from "react-router";
import {updateMessageRequestAction} from "../Chat/actions";
import {hideEditMessageAction} from "./actions";

interface EditPageStateProps {
  message?: IMessage
}

interface EditPageProps extends EditPageStateProps {
  hideEditMessage(): void,
  updateMessage(message: IMessage): void
}

const EditPage: React.FunctionComponent<EditPageProps> = (
  {
    hideEditMessage,
    message,
    updateMessage
  }
) => {
  const [text, setText] = useState<string>(message ? message.text : '');
  const textareaRef = useRef<HTMLTextAreaElement>(null);

  const handleChange = (value: string): void => {
    setText(value);
  };

  const handleUpdate = () => {
    if (text.trim() && message) {
      updateMessage({...message, text});
    }
  };

  const handleFormUpdate = (e: React.FormEvent) => {
    e.preventDefault();
    handleUpdate();
  };

  const handleTextAreaKeyPress = (e: React.KeyboardEvent<HTMLTextAreaElement>): void => {
    if (e.key === 'Enter' && !e.shiftKey) {
      e.preventDefault();
      handleUpdate();
    }
  };

  const focusTextarea = (): void => {
    if (textareaRef.current) {
      // to focus on textarea and put cursor to the end
      textareaRef.current.focus();
      const tmp: string = textareaRef.current.innerHTML;
      textareaRef.current.value = '';
      textareaRef.current.value = tmp;
    }
  };

  useEffect(() => {

    const handleKeyboardClose = (e: KeyboardEvent): void => {
      if (e.key === 'Escape') {
        hideEditMessage();
      }
    };
    focusTextarea();
    document.addEventListener('keydown', handleKeyboardClose);
    return () => {
      document.removeEventListener('keydown', handleKeyboardClose);
    };
  });

  if (!message) {
    return <Redirect to="/"/>
  }

  return (
    <div className="form-wrapper">
      <form className="form" onSubmit={e => handleFormUpdate(e)}>
        <div className="form-header">
          <h1>Edit message</h1>
        </div>
        <div className="form-row">
          <textarea
            className="form-textarea"
            onChange={e => handleChange(e.target.value)}
            onKeyDown={e => handleTextAreaKeyPress(e)}
            value={text}
            ref={textareaRef}
          />
        </div>
        <div className="form-row form-buttons">
          <ConfirmButton title="Update" isBlocked={text.trim() === ''} callback={() => {
          }} submit={true}/>
          <SimpleButton title="Cancel" callback={() => hideEditMessage()} />
        </div>
      </form>
    </div>
  );
};

const mapStateToProps = (rootState: IRootState): EditPageStateProps => ({
  message: rootState.edit?.message
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  hideEditMessage: () => dispatch(hideEditMessageAction()),
  updateMessage: (message: IMessage) => dispatch(updateMessageRequestAction(message))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditPage);
