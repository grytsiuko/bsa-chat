import { AnyAction } from 'redux';
import { IMessage } from '../../service/messageService';
import {SET_MESSAGE} from './actionTypes';
import {LOGOUT_USER_SUCCEEDED} from "../LoginPage/actionTypes";

export interface IEditState {
  message?: IMessage,
}

const editReducer = (state: IEditState = {}, action: AnyAction) => {
  switch (action.type) {
    case SET_MESSAGE:
      return {
        ...state,
        message: action.message
      };
    case LOGOUT_USER_SUCCEEDED:
      return {};
    default:
      return state;
  }
};

export default editReducer;
