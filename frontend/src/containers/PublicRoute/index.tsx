import React from 'react';
import {Route} from 'react-router-dom';
import {connect} from 'react-redux';
import {IRootState} from "../../store";
import {IUser} from "../../service/userService";

interface PublicRouteStateProps {
  user?: IUser
}

interface PublicRouteProps extends PublicRouteStateProps {
  component: React.FunctionComponent,
  path: string,
  exact?: boolean
}

const PublicRoute: React.FunctionComponent<PublicRouteProps> = (
  {
    component,
    path,
    exact
  }
) => (
  <Route
    path={path}
    exact={exact}
    component={component}
  />
);

const mapStateToProps = (rootState: IRootState): PublicRouteStateProps => ({
  user: rootState?.login?.user
});

export default connect(mapStateToProps)(PublicRoute);
