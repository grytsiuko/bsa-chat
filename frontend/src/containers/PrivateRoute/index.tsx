import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {connect} from 'react-redux';
import {IRootState} from "../../store";
import {IUser} from "../../service/userService";

interface PrivateRouteStateProps {
  user?: IUser
}

interface PrivateRouteProps extends PrivateRouteStateProps {
  component: React.FunctionComponent,
  path: string,
  exact?: boolean
}

const PrivateRoute: React.FunctionComponent<PrivateRouteProps> = (
  {
    component,
    path,
    exact
  }
) => {
  if (!localStorage.getItem("token")) {
    return <Redirect to="/login" />
  }

  return (
    <Route
      path={path}
      exact={exact}
      component={component}
    />
  )
};

const mapStateToProps = (rootState: IRootState): PrivateRouteStateProps => ({
  user: rootState?.login?.user
});

export default connect(mapStateToProps)(PrivateRoute);
