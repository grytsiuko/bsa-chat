import React from 'react';
import {connect} from 'react-redux';

import './index.css';
import {IRootState} from "../../store";
import {IUser} from "../../service/userService";
import {NavLink} from "react-router-dom";

interface MenuStateProps {
  user?: IUser
}

interface MenuProps extends MenuStateProps {
}

const Menu: React.FunctionComponent<MenuProps> = (
  {
    user
  }
) => {
  return (
    <div>
      {user &&
      <div className="menu-wrapper">
        <span className="menu-item menu-item-plain menu-item-bold">{user.username}</span>
        <NavLink exact to="/" className="menu-item" activeClassName="menu-item-active">Chat</NavLink>
        {user.isAdmin && <NavLink to="/users" className="menu-item" activeClassName="menu-item-active">Users</NavLink>}
        <NavLink to="/logout" className="menu-item" activeClassName="menu-item-active">Log out</NavLink>
      </div>
      }
    </div>
  )
};

const mapStateToProps = (rootState: IRootState): MenuStateProps => ({
  user: rootState?.login?.user
});

export default connect(mapStateToProps)(Menu);
