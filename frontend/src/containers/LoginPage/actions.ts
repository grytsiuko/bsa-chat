import {AnyAction} from 'redux';
import {
  LOAD_CURRENT_USER_REQUESTED,
  LOGIN_USER_FAILED,
  LOGIN_USER_REQUESTED,
  LOGIN_USER_SUCCEEDED,
  LOGOUT_USER_SUCCEEDED
} from './actionTypes';
import {ILoginRequest} from "../../service/authService";
import {IUser} from "../../service/userService";

// REQUESTED

export const loginUserRequestAction = (auth: ILoginRequest): AnyAction => ({
  type: LOGIN_USER_REQUESTED,
  auth
});

export const loadCurrentUserRequestAction = (): AnyAction => ({
  type: LOAD_CURRENT_USER_REQUESTED
});

// SUCCEEDED

export const loginUserAction = (user: IUser): AnyAction => ({
  type: LOGIN_USER_SUCCEEDED,
  user
});

export const logoutUserAction = (): AnyAction => ({
  type: LOGOUT_USER_SUCCEEDED
});

// FAILED

export const loginUserFailedAction = (error: string): AnyAction => ({
  type: LOGIN_USER_FAILED,
  error
});
