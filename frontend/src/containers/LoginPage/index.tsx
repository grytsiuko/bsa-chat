import React, {useState} from 'react';

import './index.css';
import ConfirmButton from "../../components/ConfirmButton";
import {Dispatch} from "redux";
import {connect} from "react-redux";
import {ILoginRequest} from "../../service/authService";
import {IRootState} from "../../store";
import {IUser} from "../../service/userService";
import {Redirect} from "react-router";
import {loginUserRequestAction} from "./actions";

interface LoginPageStateProps {
  user?: IUser,
  error?: string | null
}

interface LoginPageProps extends LoginPageStateProps {
  loginUser(request: ILoginRequest): void
}

const LoginPage: React.FunctionComponent<LoginPageProps> = ({loginUser, user, error}) => {
  const [username, setUsername] = useState<string>('');
  const [password, setPassword] = useState<string>('');

  if (user) {
    return user.isAdmin
      ? <Redirect to="/users" />
      : <Redirect to="/" />;
  }

  const handleChangeLogin = (value: string): void => {
    setUsername(value);
  }

  const handleChangePassword = (value: string): void => {
    setPassword(value);
  }

  const handleLogin = (e: React.FormEvent) => {
    e.preventDefault();
    if (username.trim() !== '' && password) {
      loginUser({username, password});
    }
  };

  return (
    <div className="form-wrapper">
      <form className="form" onSubmit={e => handleLogin(e)}>
        {error && <div className="form-error">{error}</div>}
        <div className="form-header">
          <h1>TeleChat</h1>
        </div>
        <div className="form-row">
          <label className="form-label">Username</label>
          <input type="text" className="form-input" value={username} onChange={e => handleChangeLogin(e.target.value)}/>
        </div>
        <div className="form-row">
          <label className="form-label">Password</label>
          <input type="password" className="form-input" value={password}
                 onChange={e => handleChangePassword(e.target.value)}/>
        </div>
        <div className="form-row form-buttons">
          <ConfirmButton title="Log in" isBlocked={username.trim() === '' || !password} callback={() => {
          }} submit={true}/>
        </div>
      </form>
    </div>
  );
};

const mapStateToProps = (rootState: IRootState): LoginPageStateProps => ({
  user: rootState.login?.user,
  error: rootState.login?.error
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  loginUser: (request: ILoginRequest) => dispatch(loginUserRequestAction(request))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
