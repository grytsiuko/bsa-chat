import {call, put, all, takeEvery} from 'redux-saga/effects';

import {AnyAction} from "redux";
import {IAuth} from "../../service/authService";
import * as authService from "../../service/authService";
import {loginUserAction, loginUserFailedAction, logoutUserAction} from "./actions";
import {LOAD_CURRENT_USER_REQUESTED, LOGIN_USER_REQUESTED} from "./actionTypes";
import {IUser} from "../../service/userService";
import {setLoadingAction, stopLoadingAction} from "../App/actions";

export function* loginUser(action: AnyAction) {
  try {
    yield put(setLoadingAction());
    const result: IAuth = yield call(authService.loginRequest, action.auth);
    localStorage.setItem("token", result.token);
    yield put(loginUserAction(result.user));
  } catch (e) {
    yield put(loginUserFailedAction(e.message));
  } finally {
    yield put(stopLoadingAction());
  }
}

function* watchLoginUser() {
  yield takeEvery(LOGIN_USER_REQUESTED, loginUser);
}

export function* loadCurrentUser() {
  try {
    yield put(setLoadingAction());
    const result: IUser = yield call(authService.loadCurrentRequest);
    yield put(loginUserAction(result));
  } catch (e) {
    yield put(logoutUserAction());
  } finally {
    yield put(stopLoadingAction());
  }
}

function* watchLoadCurrentUser() {
  yield takeEvery(LOAD_CURRENT_USER_REQUESTED, loadCurrentUser);
}

export default function* loginSagas() {
  yield all([
    watchLoginUser(),
    watchLoadCurrentUser()
  ]);
}

