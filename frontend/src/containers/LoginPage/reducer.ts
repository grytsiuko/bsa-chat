import {AnyAction} from 'redux';
import {LOGIN_USER_FAILED, LOGIN_USER_REQUESTED, LOGIN_USER_SUCCEEDED, LOGOUT_USER_SUCCEEDED} from './actionTypes';
import {IUser} from "../../service/userService";

export interface ILoginState {
  user?: IUser
  error?: string | null
}

const loginReducer = (state: ILoginState = {}, action: AnyAction) => {
  switch (action.type) {
    case LOGIN_USER_REQUESTED:
      return {
        ...state,
        error: null
      };
    case LOGIN_USER_SUCCEEDED:
      return {
        ...state,
        user: action.user
      };
    case LOGIN_USER_FAILED:
      return {
        ...state,
        user: null,
        error: action.error
      };
    case LOGOUT_USER_SUCCEEDED:
      localStorage.removeItem("token");
      return {
        user: null,
        error: null
      };
    default:
      return state;
  }
};

export default loginReducer;
