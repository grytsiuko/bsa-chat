import {AnyAction} from 'redux';
import {IDetailedUser, IUserArray} from "../../service/userService";
import {
  ADD_USER_REQUESTED,
  DELETE_USER_REQUESTED,
  LOAD_USERS_REQUESTED,
  LOAD_USERS_SUCCEEDED, UPDATE_USER_REQUESTED,
  USER_DATA_FAILED
} from "./actionTypes";

// REQUESTED

export const loadUsersRequestAction = (): AnyAction => ({
  type: LOAD_USERS_REQUESTED
});

export const deleteUserRequestAction = (id: string, currentId?: string): AnyAction => ({
  type: DELETE_USER_REQUESTED,
  id,
  currentId
});

export const addUserRequestAction = (user: IDetailedUser, redirect: any): AnyAction => ({
  type: ADD_USER_REQUESTED,
  user,
  redirect
});

export const updateUserRequestAction = (user: IDetailedUser, redirect: any, currentId?: string): AnyAction => ({
  type: UPDATE_USER_REQUESTED,
  user,
  currentId,
  redirect
});

// SUCCEEDED

export const loadUsersSucceededAction = (users: IUserArray | null): AnyAction => ({
  type: LOAD_USERS_SUCCEEDED,
  users
});

// FAILED

export const userDataFailedAction = (message: string): AnyAction => ({
  type: USER_DATA_FAILED,
  message
});
