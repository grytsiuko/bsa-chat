import {call, put, all, takeEvery} from 'redux-saga/effects';

import {loadUsersRequestAction, loadUsersSucceededAction, userDataFailedAction} from "./actions";
import {ADD_USER_REQUESTED, DELETE_USER_REQUESTED, LOAD_USERS_REQUESTED, UPDATE_USER_REQUESTED} from "./actionTypes";
import { IUserArray} from "../../service/userService";
import * as userService from "../../service/userService";
import {AnyAction} from "redux";
import {loginUserAction, logoutUserAction} from "../LoginPage/actions";
import {setLoadingAction, stopLoadingAction} from "../App/actions";

export function* loadUsers() {
  yield put(setLoadingAction());
  const users: IUserArray = yield call(userService.getUsers);
  yield put(loadUsersSucceededAction(users));
  yield put(stopLoadingAction());
}

function* watchLoadUsers() {
  yield takeEvery(LOAD_USERS_REQUESTED, loadUsers);
}

export function* deleteUser(action: AnyAction) {
  yield put(setLoadingAction());
  yield call(userService.deleteUser, action.id);
  if (action.id === action.currentId) {
    yield put(logoutUserAction());
  } else {
    yield put(loadUsersRequestAction());
  }
  yield put(stopLoadingAction());
}

function* watchDeleteUser() {
  yield takeEvery(DELETE_USER_REQUESTED, deleteUser);
}

export function* addUser(action: AnyAction) {
  try {
    yield put(setLoadingAction());
    yield call(userService.addUser, action.user);
    yield put(loadUsersRequestAction());
    yield call(action.redirect);
  } catch (e) {
    yield put(userDataFailedAction(e.message));
  } finally {
    yield put(stopLoadingAction());
  }
}

function* watchAddUser() {
  yield takeEvery(ADD_USER_REQUESTED, addUser);
}

export function* updateUser(action: AnyAction) {
  try {
    yield put(setLoadingAction());
    yield call(userService.updateUser, action.user);
    if (action.user.id === action.currentId) {
      yield put(loginUserAction(action.user));
    }
    yield put(loadUsersRequestAction());
    yield call(action.redirect);
  } catch (e) {
    yield put(userDataFailedAction(e.message));
  } finally {
    yield put(stopLoadingAction());
  }
}

function* watchUpdateUser() {
  yield takeEvery(UPDATE_USER_REQUESTED, updateUser);
}

export default function* usersSagas() {
  yield all([
    watchLoadUsers(),
    watchDeleteUser(),
    watchAddUser(),
    watchUpdateUser()
  ]);
}

