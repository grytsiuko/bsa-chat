import React from 'react';

import './index.css';
import {connect} from "react-redux";
import {IRootState} from "../../store";
import {IUser, IUserArray} from "../../service/userService";
import {deleteUserRequestAction, loadUsersRequestAction} from "./actions";
import {Dispatch} from "redux";
import Icon from "../../components/Icon";
import {useHistory} from "react-router";
import {clearEditUserAction, setEditUserAction} from "../EditUserPage/actions";

interface UsersPageStateProps {
  users?: IUserArray
  user?: IUser
}

interface UsersPageProps extends UsersPageStateProps {
  loadUsers(): void,
  deleteUser(id: string, currentId?: string): void,
  setEditUser(user: IUser): void
  clearEditUser(): void
}

const UsersPage: React.FunctionComponent<UsersPageProps> = (
  {
    users,
    user,
    loadUsers,
    deleteUser,
    setEditUser,
    clearEditUser
  }
) => {
  const history = useHistory();

  if (!users) {
    loadUsers();
  }

  const handleDelete = (id: string): void => {
    deleteUser(id, user?.id);
  }

  const handleEdit = (user: IUser): void => {
    setEditUser(user);
    history.push("/users/edit");
  }

  const handleAdd = (): void => {
    clearEditUser();
    history.push("/users/edit");
  }

  return (
    <div className="list-wrapper">
      <h2 className="list-title">Users list</h2>
      <div className="list-item list-item-center">
        <div className="item-block">
          <Icon icon="fas fa-plus" active={false} callback={() => handleAdd()}/>
        </div>
      </div>
      {users?.map(u => (
        <div className="list-item" key={u.id}>
          <div className="item-block">
            <img alt={u.username} src={u.avatar} className="list-avatar"/>
            <div className="list-bold">{u.username}</div>
            {u.isAdmin && <div className="list-label">admin</div>}
          </div>
          <div className="item-block">
            <Icon icon="fas fa-edit" active={false} callback={() => handleEdit(u)}/>
            <Icon icon="fas fa-trash" active={false} callback={() => handleDelete(u.id)}/>
          </div>
        </div>
      ))}
    </div>
  );
};

const mapStateToProps = (rootState: IRootState): UsersPageStateProps => ({
  users: rootState?.users?.users,
  user: rootState.login?.user
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  loadUsers: () => dispatch(loadUsersRequestAction()),
  deleteUser: (id: string, currentId?: string) => dispatch(deleteUserRequestAction(id, currentId)),
  setEditUser: (user: IUser) => dispatch(setEditUserAction(user)),
  clearEditUser: () => dispatch(clearEditUserAction())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersPage);
