import { AnyAction } from 'redux';
import {LOAD_USERS_SUCCEEDED} from './actionTypes';
import {IUserArray} from "../../service/userService";
import {LOGOUT_USER_SUCCEEDED} from "../LoginPage/actionTypes";

export interface IUsersState {
  users?: IUserArray
}

const usersReducer = (state: IUsersState = {}, action: AnyAction) => {
  switch (action.type) {
    case LOAD_USERS_SUCCEEDED:
      return {
        ...state,
        users: action.users
      };
    case LOGOUT_USER_SUCCEEDED:
      return {};
    default:
      return state;
  }
};

export default usersReducer;
