import { AnyAction } from 'redux';
import {SET_USER} from "./actionTypes";
import {IUser} from "../../service/userService";
import {LOGOUT_USER_SUCCEEDED} from "../LoginPage/actionTypes";
import {ADD_USER_REQUESTED, UPDATE_USER_REQUESTED, USER_DATA_FAILED} from "../UsersPage/actionTypes";

export interface IEditUserState {
  user?: IUser
  error?: string | null
}

const editUserReducer = (state: IEditUserState = {}, action: AnyAction) => {
  switch (action.type) {
    case ADD_USER_REQUESTED:
    case UPDATE_USER_REQUESTED:
      return {
        ...state,
        error: null
      }
    case USER_DATA_FAILED:
      return {
        ...state,
        error: action.message
      };
    case SET_USER:
      return {
        ...state,
        user: action.user,
        error: null
      };
    case LOGOUT_USER_SUCCEEDED:
      return {};
    default:
      return state;
  }
};

export default editUserReducer;
