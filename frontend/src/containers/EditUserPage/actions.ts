import {AnyAction} from 'redux';

import {IUser} from "../../service/userService";
import {SET_USER} from "./actionTypes";

export const setEditUserAction = (user: IUser): AnyAction => ({
  type: SET_USER,
  user
});

export const clearEditUserAction = (): AnyAction => ({
  type: SET_USER,
  user: null
});
