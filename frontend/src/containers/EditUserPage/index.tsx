import React, {useState} from 'react';

import ConfirmButton from '../../components/ConfirmButton';
import SimpleButton from '../../components/SimpleButton';
import {IRootState} from "../../store";
import {Dispatch} from "redux";
import {connect} from "react-redux";
import {IDetailedUser, IUser} from "../../service/userService";
import {useHistory} from "react-router";
import {loginUserAction} from "../LoginPage/actions";
import {clearEditUserAction} from "./actions";
import {addUserRequestAction, updateUserRequestAction} from "../UsersPage/actions";

interface EditUserPageStateProps {
  user?: IUser,
  currentUser?: IUser,
  error?: string | null
}

interface EditUserPageProps extends EditUserPageStateProps {
  addUser(user: IDetailedUser, redirect: any): void,
  updateUser(user: IDetailedUser, redirect: any, currentId?: string): void,
  clearEditUser(): void,
}

const EditUserPage: React.FunctionComponent<EditUserPageProps> = (
  {
    error,
    addUser,
    updateUser,
    clearEditUser,
    user,
    currentUser
  }
) => {
  const history = useHistory();
  const [username, setUsername] = useState<string>(user ? user.username : '');
  const [avatar, setAvatar] = useState<string>(user ? user.avatar : '');
  const [password, setPassword] = useState<string>('');
  const [isAdmin, setIsAdmin] = useState<boolean>(user ? user.isAdmin : false);

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    const redirect = () => history.push("/users");
    if (!user) {
      addUser({id: null, username, avatar, password, isAdmin}, redirect);
    } else {
      updateUser({id: user.id, username, avatar, password, isAdmin}, redirect, currentUser?.id)
    }
  };

  const handleCancel = () => {
    clearEditUser();
    history.push("/users");
  }

  return (
    <div className="form-wrapper">
      <form className="form" onSubmit={e => handleSubmit(e)}>
        {error && <div className="form-error">{error}</div>}
        <div className="form-header">
          <h1>{user ? "Edit User" : "Add User"}</h1>
        </div>
        <div className="form-row">
          <label className="form-label">Username</label>
          <input type="text" className="form-input" value={username} onChange={e => setUsername(e.target.value)}/>
        </div>
        <div className="form-row">
          <label className="form-label">Avatar</label>
          <input type="text" className="form-input" value={avatar} onChange={e => setAvatar(e.target.value)}/>
        </div>
        <div className="form-row">
          <label className="form-label"> New Password</label>
          <input type="password" className="form-input" value={password} onChange={e => setPassword(e.target.value)}/>
        </div>
        <div className="form-row">
          <label className="form-label">Admin</label>
          <input type="checkbox" className="form-input" checked={isAdmin} onChange={e => setIsAdmin(e.target.checked)}/>
        </div>
        <div className="form-row form-buttons">
          <ConfirmButton title="Save" isBlocked={false} callback={() => {
          }} submit={true}/>
          <SimpleButton title="Cancel" callback={() => handleCancel()}/>
        </div>
        {user && <div>*Leave password empty to keep the same</div>}
      </form>
    </div>
  );
};

const mapStateToProps = (rootState: IRootState): EditUserPageStateProps => ({
  user: rootState.editUser?.user,
  currentUser: rootState.login?.user,
  error: rootState.editUser?.error
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  addUser: (user: IDetailedUser, redirect: any) => dispatch(addUserRequestAction(user, redirect)),
  updateUser: (user: IDetailedUser, redirect: any, currentId?: string) => dispatch(updateUserRequestAction(user, redirect, currentId)),
  clearEditUser: () => dispatch(clearEditUserAction()),
  updateLoginUser: (user: IUser) => dispatch(loginUserAction(user))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditUserPage);
