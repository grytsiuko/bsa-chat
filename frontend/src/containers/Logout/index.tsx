import React from 'react';

import {Dispatch} from "redux";
import {connect} from "react-redux";
import {Redirect} from "react-router";
import {logoutUserAction} from "../LoginPage/actions";

interface LogoutStateProps {
}

interface LogoutProps extends LogoutStateProps {
  logoutUser(): void
}

const Logout: React.FunctionComponent<LogoutProps> = ({logoutUser}) => {
  logoutUser();
  return <Redirect to="/login" />;
};

const mapDispatchToProps = (dispatch: Dispatch) => ({
  logoutUser: () => dispatch(logoutUserAction())
});

export default connect(
  null,
  mapDispatchToProps
)(Logout);
