import {AnyAction} from 'redux';
import {SET_LOADING} from './actionTypes';

export const setLoadingAction = (): AnyAction => ({
  type: SET_LOADING,
  loading: true
});

export const stopLoadingAction = (): AnyAction => ({
  type: SET_LOADING,
  loading: false
});
