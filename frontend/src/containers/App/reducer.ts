import { AnyAction } from 'redux';
import {LOGOUT_USER_SUCCEEDED} from "../LoginPage/actionTypes";
import {SET_LOADING} from "./actionTypes";

export interface IAppState {
  loading?: boolean,
}

const appReducer = (state: IAppState = {}, action: AnyAction) => {
  switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        loading: action.loading
      };
    case LOGOUT_USER_SUCCEEDED:
      return {};
    default:
      return state;
  }
};

export default appReducer;
