import React from 'react';
import {Switch} from "react-router";
import Chat from "../Chat";
import Login from "../LoginPage";
import {IRootState} from "../../store";
import {Dispatch} from "redux";
import {loadCurrentUserRequestAction} from "../LoginPage/actions";
import {connect} from "react-redux";
import {IUser} from "../../service/userService";
import PrivateRoute from "../PrivateRoute";
import PublicRoute from "../PublicRoute";
import Spinner from "../../components/Spinner";
import EditPage from "../EditPage";
import UsersPage from "../UsersPage";
import AdminRoute from "../AdminRoute";
import EditUserPage from "../EditUserPage";
import Menu from "../Menu";
import Logout from "../Logout";

interface AppStateProps {
  user?: IUser,
  loading?: boolean
}

interface AppProps extends AppStateProps {
  loadCurrentUser(): void
}

const App: React.FunctionComponent<AppProps> = ({user, loadCurrentUser, loading}) => {

  if (localStorage.getItem("token") && !user) {
    loadCurrentUser();
    return <Spinner/>
  }

  return (
    <div className="app">
      <Menu />
      <Switch>
        <PrivateRoute exact path="/edit" component={EditPage}/>
        <AdminRoute exact path="/users" component={UsersPage}/>
        <AdminRoute exact path="/users/edit" component={EditUserPage}/>
        <PublicRoute exact path="/login" component={Login}/>
        <PublicRoute exact path="/logout" component={Logout}/>
        <PrivateRoute exact path="/" component={Chat}/>
      </Switch>
      {loading && <Spinner />}
    </div>
  );
};

const mapStateToProps = (rootState: IRootState): AppStateProps => ({
  user: rootState?.login?.user,
  loading: rootState.app?.loading
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  loadCurrentUser: () => dispatch(loadCurrentUserRequestAction())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
