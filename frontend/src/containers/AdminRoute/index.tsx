import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {connect} from 'react-redux';
import {IRootState} from "../../store";
import {IUser} from "../../service/userService";

interface AdminRouteStateProps {
  user?: IUser
}

interface AdminRouteProps extends AdminRouteStateProps {
  component: React.FunctionComponent,
  path: string,
  exact?: boolean
}

const AdminRoute: React.FunctionComponent<AdminRouteProps> = (
  {
    user,
    component,
    path,
    exact
  }
) => {
  if (!user || !user.isAdmin) {
    return <Redirect to="/" />
  }

  return (
    <Route
      path={path}
      exact={exact}
      component={component}
    />
  )
};

const mapStateToProps = (rootState: IRootState): AdminRouteStateProps => ({
  user: rootState?.login?.user
});

export default connect(mapStateToProps)(AdminRoute);
