import callApi from "./apiHelper";

export interface IUser {
  id: string,
  username: string,
  avatar: string,
  isAdmin: boolean
}

export interface IDetailedUser {
  id: string | null,
  username: string,
  avatar: string,
  isAdmin: boolean
  password: string
}

export type IUserArray = Array<IUser>

export const getUsers = async (): Promise<IUserArray> => {
  const response = await callApi({
    endpoint: 'http://localhost:5555/api/users/',
    method: 'GET'
  });
  return response.json();
};

export const addUser = async (request: IDetailedUser): Promise<void> => {
  await callApi({
    endpoint: `http://localhost:5555/api/users/`,
    request,
    method: 'POST'
  });
};

export const updateUser = async (request: IDetailedUser): Promise<void> => {
  await callApi({
    endpoint: `http://localhost:5555/api/users/`,
    request,
    method: 'PUT'
  });
};

export const deleteUser = async (id: string): Promise<void> => {
  await callApi({
    endpoint: `http://localhost:5555/api/users/${id}`,
    method: 'DELETE'
  });
};
