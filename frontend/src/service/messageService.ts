import callApi from './apiHelper';

export interface IMessage {
  id: string,
  userId: string,
  avatar: string,
  user: string,
  text: string,
  createdAt: string,
  editedAt: string,
  likedByMe: boolean,
  likes: number
}

export type IMessageArray = Array<IMessage>

export const getMessages = async (): Promise<IMessageArray> => {
  const response = await callApi({
    endpoint: 'http://localhost:5555/api/messages/',
    method: 'GET'
  });
  return response.json();
};

export const addMessage = async (text: string): Promise<IMessage> => {
  const response = await callApi({
    endpoint: 'http://localhost:5555/api/messages/',
    request: {text},
    method: 'POST'
  });
  return response.json();
};

export const updateMessage = async (message: IMessage): Promise<IMessage> => {
  const response = await callApi({
    endpoint: 'http://localhost:5555/api/messages/',
    request: {id: message.id, text: message.text},
    method: 'PUT'
  });
  return response.json();
};

export const deleteMessage = async (id: string): Promise<void> => {
  await callApi({
    endpoint: `http://localhost:5555/api/messages/${id}`,
    method: 'DELETE'
  });
};

export const toggleLikeMessage = async (message: IMessage): Promise<number> => {
  const response = await callApi({
    endpoint: `http://localhost:5555/api/messages/reactions`,
    request: {messageId: message.id},
    method: 'PUT'
  });
  return Number(await response.text());
};
