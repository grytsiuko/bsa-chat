interface IApiArgs {
  endpoint: string,
  request?: any,
  method: string
}

const getFetchArgs = (args:IApiArgs): RequestInit => {
  const headers: HeadersInit = {};
  const token = localStorage.getItem('token');
  if (token) {
    headers.Authorization = token;
  }
  let body;
  if (args.request) {
    body = JSON.stringify(args.request);
    headers['Content-Type'] = 'application/json';
    headers.Accept = 'application/json';
  }
  return {
    method: args.method,
    headers,
    ...(args.request === 'GET' ? {} : { body })
  };
}

export default async function callApi(args: IApiArgs): Promise<Response> {
  const response: Response = await fetch(
    args.endpoint,
    getFetchArgs(args)
  );
  if (!response.ok) {
    throw new Error(await response.text());
  }
  return response;
}
