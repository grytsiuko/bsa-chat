import callApi from './apiHelper';
import {IUser} from "./userService";

export interface IAuth {
  token: string,
  user: IUser
}

export interface ILoginRequest {
  username: string,
  password: string
}

export const loginRequest = async (request: ILoginRequest): Promise<IAuth> => {
  const response = await callApi({
    endpoint: 'http://localhost:5555/api/auth/login',
    request,
    method: 'POST'
  });
  return response.json();
};

export const loadCurrentRequest = async (): Promise<IUser> => {
  const response = await callApi({
    endpoint: 'http://localhost:5555/api/users/current',
    method: 'GET'
  });
  return response.json();
};
